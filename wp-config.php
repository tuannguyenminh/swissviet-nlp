<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tnm_nlp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6W+swnwsX+!}%tqgQ;[%7lC4c>svZG#bLja;;E+4$b@<w,b+ %r8)(O<j-G(O%8*');
define('SECURE_AUTH_KEY',  'LMwR36R[of#;o,)J~*Rvv81rW8&ZP;S]h^o!o[>T66:C*7l;Z^x$=<Fb*;4b|FHC');
define('LOGGED_IN_KEY',    'k_rl]~-$0PfGV+_87m`jKq^=HMIU|m4e}EDrJ3a$93#zQq%i{^AEV:yb,K(:<TN~');
define('NONCE_KEY',        'w(| 3{dZMM4}zK?k:Jh4l]Ykf`pB>b&#+2B=vQYBlwW!X0&v.1X>F)g[4-gR!pZ_');
define('AUTH_SALT',        'GN~:GS-GfULl29c/nou5^x2cS1Wd$4fy&(C%E0Pp+mI>>Ktq$Do%PN8R#wr}gY-k');
define('SECURE_AUTH_SALT', 'Q,>#=.ewHk>JTqg]yeu,t~-@:nh5Zz4l}rsVJ8&Qu&&kT;Xkn]S~%<U]TFj?p+j~');
define('LOGGED_IN_SALT',   'voc:$mmoK,_;w(aOQ3Z@6eL8~)0p>9+N&<$Pr[0^Iw|0_+WK*hRA.{.:+J&!q8r?');
define('NONCE_SALT',       'YiY ayMD?[]gyd< 6&&e@#F+bNpz|aw6>O #h[!^w^kp6+J,+duC%s2ApG/`qax3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
