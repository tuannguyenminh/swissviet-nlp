=== White Label ===
Contributors: morganhvidt, whitelabelwp
Tags: white label, custom admin, dashboard, customize admin, custom login, custom login page, login rebrand, branding, change logo, custom branding, custom logo, change admin, clients, whitelabel, custom widgets, customize wordpress, white label cms, hide plugin, hide plugins, hide menus, clients, admin
Author URI: https://whitewp.com
Author: Morgan Hvidt
Donate link: https://www.paypal.me/morganhvidt/
Requires at least: 4.0
Tested up to: 5.2
Requires PHP: 5.6
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

White Label allows users to customize login logo & background, admin area, dashboard, custom widgets.

== Description ==

White Label WordPress lets administrators change the WordPress branding to bring client's websites to life! You can add a custom logo, custom login, create a complete custom WordPress dashboard with your own widget and other features to help clients use WordPress.

https://www.youtube.com/watch?v=n3zi79lM9yM

## New Plugin!
We just released White Label and we're excited improve it and bring you **new features**! Give it a go and let us know what you think!
Make sure you read our [Getting Started Guide](https://whitewp.com/should-i-rebrand-wordpress-for-clients-and-how/ "Getting Started Guide"), We will show you how to rebrand WordPress!

## Rebrand WordPress with your branding
Sometimes clients have never heard of WordPress before, which can lean to confusion. Stay professional and be a cut above the rest by White Labelling the WP sites that you create for clients with your own branding. With White Label, it's easy to customize your WordPress login screen and add your own logo and custom background image.

## Support focused
Create your own custom dashboard to make client handover so much easier. Add your own informative dashboard widget to improve client support with easily accessible tutorials, FAQs, and instructions right there in the dashboard. You can even add images and video!

You can also easily add a live chat to the WordPress backend.

## White Label Features

White Label is made for those who want to customize their WordPress site. The free version is particularly good for those with clients who have the Editor role.

* Easy replace the default logo with your own on the WordPress login screen
* Custom background color for the login screen
* Custom background image for the WordPress login screen
* Set up a custom dashboard Widget with the visual editor
* Custom a WordPress dashboard page with the visual editor
* Add live chat or custom Javascript script to the admin area
* Change the welcome message a.k.a. 'Howdy'
* Hide the WordPress admin bar logo
* Replace the WordPress admin bar logo with your own
* Change the admin footer credit
* Hide White Label from those with an editor role

Want to hide specific plugins (including White Label) from multiple administrators?
If your site has client admins then look at [White Label Pro](https://whitewp.com "White Label WordPress")

## White Label Pro
The White Label Pro Plugin for WordPress is made for professionals who require an extra level of customization. Handle multiple administrators on one site.

* Select which Administrators are allowed to see the White Label Plugin
* Hide plugins from other administrators
* Change WordPress Email address and sender name
* Custom welcome panel in the dashboard for admins only
* Remove all default WordPress dashboard widgets
* Exciting upcoming features and development
* Premium Support

Read more [White Label Pro](https://whitewp.com "White Label WordPress") here!


A 5 star review would be much appreciated if our plugin has improved your site! If you have any issues at all, please do not hesitate to [contact us](https://wordpress.org/support/plugin/white-label "contact support") and we'll do our best to help you out!


== Installation ==
While label plugin Installation Instructions:

Want to add your custom logo straight way?

1. Upload the White Label plugin to your /wp-content/plugins/ directory or through the Plugin admin section under "add new"
2. Activate the plugin through the ‘Plugins’ menu in WordPress
3. Configure your settings
4. That's it!

== Frequently Asked Questions ==

= Can I use my logo on the WordPress login page? =
Yes you can! Just upload the logo under the Settings page and save. Select the correct height and width. That's all!

= Would this plugin work for my agency? =
Absolutely. White Labelling your WordPress sites will work great. Only administrators can see the White Label menu and plugin.

= Can this plugin hide other plugins =
White Label can hide plugins from the plugins list so clients can't break your site by deleting necessary plugins.
Have a look at https://whitewp.com/ for updates on the newest features. We are working on hiding admin menus and plugins!

= Does this white label my WordPress multisite? =
Not yet, we hope to work on that in the future.

== Screenshots ==

1. White label Custom WordPress Login Logo
2. Rebrand WordPress settings
3. Custom WordPress Dashboard
4. Custom dashboard Widget
5. Custom admin bar
6. Setup a custom dashboard widget
7. Replace Howdy with White Label


== Changelog ==

= 1.4.2 =
Fixed deprecated notice for login_headertitle to use login_headertext instead for all WordPress 5.2 and up.


= 1.4.1 =
* Tested and ready for WordPress 5.2

= 1.4.0 =
* Tested and ready for WordPress 5.1

= 1.3 =
* Fixed textfields from breaking when inline html (only applies use of "double quotes" e.g in links)
* Improved security
* Cleaned up files

= 1.2 =
* Fixed the "Howdy" welcome message to work with all site languages
* Added admin bar logo as a WordPress White Label option
* Simplified some customisation options

= 1.1 =
* Added the ability to add a custom background image to the WordPress login screen
* Fixed feature names
* Restructured the plugin

= 1.0.0 =
* White Label goes live on WordPress.org
